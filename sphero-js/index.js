const http = require('http');
const url = require('url');
const sphero = require("sphero");

// sphero UUID
const orb = sphero("e5937d383f2e419081608bf3095ec7a5");

const DEFAULT_FACE = '/default';
const HAPPY_FACE = '/happy';
const SAD_FACE = '/sad';

const AVAILABLE_FACES = new Set([DEFAULT_FACE, HAPPY_FACE, SAD_FACE]);

let connected = false;
let currentFace = DEFAULT_FACE;

orb.connect(() => {
  console.log("connected");
  connected = true;
});

executeFace = face => {
  if (face === HAPPY_FACE) {
    orb.color('red');
    orb.roll(100, 0); // speed = 100, forward
  } else if (face === SAD_FACE) {
    orb.color('blue');
    orb.roll(100, 180); // speed = 100, backwards
  } else {
    orb.color('white');
    orb.roll(0); // stop
  }
}

handleFace = newFace => {
  console.log('Received face:', newFace);
  if (!connected) {
    console.log('Sphero not connected');
  } else if (newFace !== currentFace) {
    currentFace = newFace;
    executeFace(newFace);
  }
}

const ok = res => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Accepted!');
    res.end();
}

const error = res => {
    res.writeHead(400, {'Content-Type': 'text/plain'});
    res.write('Error. Use /happy, /sad or /default endpoint.');
    res.end(); 
}

// create HTTP server which will receive gestures
// Example: localhost:8420/happy or /sad or /default
http.createServer((req, res) => {
  const { path } = url.parse(req.url, true);
  if (path && AVAILABLE_FACES.has(path)) {
    ok(res);
    handleFace(path);
  } else {
    error(res);
  }
}).listen(8420);
