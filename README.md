Sphero Node.js server is under sphero-js

Go to `sphero-js` and run (you need Node.js):
```
npm install
node index.js
```

Then in sphero-webcam run:
```
python DetectEmotion.py
```
And install missing libs using `pip`, probably you will need `keras`.
